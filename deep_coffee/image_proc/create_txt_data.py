import os
import argparse

 
if __name__ == "__main__":

    parser = argparse.ArgumentParser("Rotate Beans")
    parser.add_argument("--input_dir", type=str, required=True)
    parser.add_argument("--output_dir", type=str, required=True)
    parser.add_argument("--eval", type=int, required=True)
    parser.add_argument("--test", type=int, required=True)
    parser.add_argument("--train", type=int, required=True)

    args = parser.parse_args()
    # obtner la direccion de la nueva data
    path =args.input_dir
    name_file = (path.split("/")[1])

    # obtener parametros de ivicion de la data
    eval_ = args.eval
    test = args.test
    train = args.train

    dir_list = os.listdir(path)
    total = eval_+test+train
    if total==100:
    	tamano_archivos = len(dir_list)
    	size_eval = (int(tamano_archivos*(eval_/100)))
    	size_test = (int(tamano_archivos*(test/100)))
    	size_train = (int(tamano_archivos*(train/100)))
        print(f"{size_eval} - {size_train} - {size_test}")
    	
    	limite_list_test = size_eval+size_train
    	limite_list_eval = limite_list_test+size_eval
    	list_data_train = dir_list[0:size_train]
    	list_data_test = dir_list[size_train:limite_list_test]
    	list_data_eval = dir_list[limite_list_test:]

    	# generar los archivos con las direcciones de las imagen

    	with open(f'{args.output_dir}{name_file}_train.txt', 'w') as f:
		    for line in list_data_train:
		        f.write(line)
		        f.write('\n')


    	with open(f'{args.output_dir}{name_file}_test.txt', 'w') as f:
		    for line in list_data_test:
		        f.write(line)
		        f.write('\n')


    	with open(f'{args.output_dir}{name_file}_eval.txt', 'w') as f:
		    for line in list_data_eval:
		        f.write(line)
		        f.write('\n')



    	with open(f'{args.output_dir}{name_file}.txt', 'w') as f:
		    for line in dir_list:
		        f.write(line)
		        f.write('\n')

    else:
    	print("Error al separar la data. La suma de los valores debe ser %100\n")
