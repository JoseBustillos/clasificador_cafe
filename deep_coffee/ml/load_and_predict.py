import argparse

import tensorflow as tf
import cv2
import time
import numpy as np

import logging
import openpyxl

# import tensorflow_hub as hub
import cv2
import numpy
import pandas as pd
import tensorflow as tf
import matplotlib.pyplot as plt

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

if __name__ == "__main__":
    start = time.time()
    cap = cv2.VideoCapture(0)

    if not cap.isOpened():
        raise IOError("Cannot open webcam")
    loaded = tf.saved_model.load("trained_models/coffee_net_v1/20220322-014357/saved_model/")

    model = tf.saved_model.load("trained_models/coffee_net_v1/20220322-014357/saved_model/")
    coordenadas = list()
    i=0
    while True:
        ret, frame = cap.read()
        frame = cv2.resize(frame, None, fx=0.5, fy=0.5, interpolation=cv2.INTER_AREA)
        img_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        blurred = cv2.GaussianBlur(img_gray, (5, 5), 0)
        img = cv2.Laplacian(img_gray, cv2.CV_64F)
        img=np.uint8(np.absolute(img))
        sobelX = cv2.Sobel(img, cv2.CV_64F, 1, 0)
        sobelY = cv2.Sobel(img, cv2.CV_64F, 0, 1)
        sobelX = np.uint8(np.absolute(sobelX))
        sobelY = np.uint8(np.absolute(sobelY))
        sobelCombined = cv2.bitwise_or(sobelX, sobelY)
        canny = cv2.Canny(blurred, 30, 300)
        coins = frame.copy()
        (cnts, _) = cv2.findContours(canny.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        end = time.time()
        if end-start>3:
            for cnt in cnts:
                x,y,w,h = cv2.boundingRect(cnt)
                coins2 = coins[y:y+h, x:x+w]
                cv2.imwrite("roi.jpg", coins2)
                image_sample = np.expand_dims(cv2.resize(cv2.cvtColor(cv2.imread("roi.jpg"), cv2.COLOR_BGR2RGB), (224, 224)), axis=0)/255.0
                prediction = model.signatures["serving_default"](tf.constant(image_sample, dtype=tf.float32))
                for _, values in prediction.items():
                    predictions = values.numpy()[0][0]
                    probability = values.numpy()[0][1]
                    if predictions>0.76 and probability>0.22:
                        i+=1
                        cv2.rectangle(coins,(x,y),(x+w,y+h),(0,0,255),2)
                        data = (i,x,y)
                        coordenadas.append(data)
        minutos=1
        segundos = minutos*60
        if not end-start <= segundos:
            wb = openpyxl.Workbook()
            hoja = wb.active
            # Crea la fila del encabezado con los títulos
            hoja.append(('ID', 'X', 'Y'))
            for producto in coordenadas:
                hoja.append(producto)
            wb.save('coordenadas.xlsx')
        if end-start > segundos:
            i=0
            start=time.time()
            coordenadas=[]
        cv2.imshow('Input', coins)
        c = cv2.waitKey(1)
    cap.release()
    cv2.destroyAllWindows()

