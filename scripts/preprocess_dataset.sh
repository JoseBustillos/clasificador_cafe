echo "Create tfrecords"
docker run \
    -v ${PWD}/dataset:/dataset \
    deep_coffee \
    python -m deep_coffee.ml.images_to_tfrecords \
    --output_dir /app/dataset/tfrecords \
    --tft_artifacts_dir /app/dataset/tft_artifacts \
    --good_beans_dir /app/dataset/good \
    --good_beans_list_train /app/dataset/protocol/good_train.txt \
    --good_beans_list_eval /app/dataset/protocol/good_eval.txt \
    --good_beans_list_test /app/dataset/protocol/good_test.txt \
    --bad_beans_dir /app/dataset/bad \
    --bad_beans_list_train /app/dataset/protocol/bad_train.txt \
    --bad_beans_list_eval /app/dataset/protocol/bad_eval.txt \
    --bad_beans_list_test /app/dataset/protocol/bad_test.txt \
    --image_dim 224 \
    --n_shards 25 \
    --ext jpg \
    --temp-dir /tmp
