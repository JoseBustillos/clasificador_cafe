FROM tensorflow/tensorflow:2.0.0-gpu-py3

WORKDIR /app


ENV APP_ROOT /app/
ENV CONFIG_ROOT /config
# General dependencies

RUN mkdir ${CONFIG_ROOT}
WORKDIR ${APP_ROOT}

RUN apt-get update && apt-get install -y
RUN apt install -y  libsm6 libxext6 libxrender-dev make
RUN apt-get update
RUN pip install --upgrade pip
COPY requirements.txt ${CONFIG_ROOT}/requirements.txt
RUN pip install -r ${CONFIG_ROOT}/requirements.txt
ADD . ${APP_ROOT}

EXPOSE 6000
