### Precaución
El prollecto debe levantarse en una maquina con SO Linux.
No es recomendable usar MAC con procesador M1, por el motivo que las librerias no logran realizar sus funciones pora la arquitectura del procesador.
### Preparar el entorno
Si no existe la imagen creada, primeramente se debe ejecutar el siguiente comando para esto debemos tener instalado "make"

#### Comando instalación "make"
```
sudo apt-get update
sudo apt-get -y install make
```
Al momento de ejecutar dicho comando lo primero sera leer el archivo Makefile que posee las instrucciones para descargar data para entrenamiento, testeo y validación del modelo. Luego generar la imgane con todos los requerimientos necesarios para el proyecto.
``` 
make install
```
En el caso que exista error podemos utilizar los soguientes comandos.
### Construit la imagen docker

Si ejecuto el comando "make install", no debe ejecutar las siguiente.

Dicho comando permite crear la imagen con todos los requerimientos, donde lee el archivo "Dockerfile", y ejecuta todfas las instrucciones como actualizacion de paquetes e instalación.
``` 
docker build . -t deep_coffee
```

### Probar si la imagen se construyo correctament
Comando que permite construir la imagen
``` 
sudo docker run deep_coffee
```

### Ingresar a la imagen
Permite ingresar a la imagen de forma iterable, quiere decir que permite al usuario ingresar a la imagen e interactuar. Además, actualiza los archivos como el dataset descargado.
Envia la data descargada en el computador a la imagen levantando en el puerto 6006:6006
``` 
docker run -it \
-v ${PWD}/deep_coffee:/src/deep_coffee \
-v ${PWD}/test:/src/test \
-v ${PWD}/dataset:/dataset \
-v ${PWD}/trained_models:/trained_models \
-v ${PWD}/keras_pretrained_models:/root/.keras/models/ \
-p 6006:6006 \
--rm --gpus all deep_coffee bash
```

#### Ejecutar pruebas 
Permite ejecutar pruebas unitarias para verificar si fue correcto la configuración de la imagen.
Dichas pruebas unitarias consiste en ejecutar los siguientes archivos.
``` 
docker run \
--rm --gpus all deep_coffee \
python -m unittest discover -s /app/test/image_proc
```

> **test_crop_beans.py** -> lectura de la imagen de prueba para ejecutar OpenCV y detectar objetos.
> 
> **test_opencv_stream.py** -> permite verificar el proceso de preprocesamiento de la imagen


#### Crear muestras de cafe
Ejecutamos el siguientes script "deep_coffee.image_proc.crop_beans", donde debemos enviar varios arguemntos, como:
> **raw_images_dir** -> dirección de las imagenes de cafe en buen estado donde se encuentran en grupos
>
> **output_dir** -> dirección de las imganes de cafe en buen estado separadas de las imagenes e grupo

Esto permite que la data descargada donde las imagenes de cafe que se encuentran en grupos, genera mas imagenes separandolas detectando el cafe y generando una nueva image que contenga un solo cafe.

``` 
docker run \
-v ${PWD}/dataset:/dataset \
--rm --gpus all deep_coffee \
python -m deep_coffee.image_proc.crop_beans \
--raw_images_dir /dataset/raw \
--output_dir /dataset/cropped
```

#### Data Augmentation

This is already performed if you previously have run `make install` 

Up to this day, only rotation is implemented

**TODO**:

* Saturation & Brightness
* Noise
* GANs

#### Rotate beans

Good beans

``` 
docker run \
-v ${PWD}/dataset:/dataset \
--rm --gpus all deep_coffee \
python -m deep_coffee.image_proc.data_aug \
--input_dir /dataset/good \
--output_dir /dataset/good \
--angle_list 45,90,135,180,225,270
```

Bad beans

``` 
docker run \
-v ${PWD}/dataset:/dataset \
--rm --gpus all deep_coffee \
python -m deep_coffee.image_proc.data_aug \
--input_dir /dataset/bad \
--output_dir /dataset/bad \
--angle_list 45,90,135,180,225,270
```

#### Generate TFRecords

This is already performed if you previously have run `make install` 

``` 
docker run \
-v ${PWD}/dataset:/dataset \
--rm --gpus all deep_coffee \
python -m deep_coffee.ml.images_to_tfrecords \
--output_dir /dataset/tfrecords \
--tft_artifacts_dir /dataset/tft_artifacts \
--good_beans_dir /dataset/good \
--good_beans_list_train /dataset/protocol/good_train.txt \
--good_beans_list_eval /dataset/protocol/good_eval.txt \
--good_beans_list_test /dataset/protocol/good_test.txt \
--bad_beans_dir /dataset/bad \
--bad_beans_list_train /dataset/protocol/bad_train.txt \
--bad_beans_list_eval /dataset/protocol/bad_eval.txt \
--bad_beans_list_test /dataset/protocol/bad_test.txt \
--image_dim 224 \
--n_shards 10 \
--ext jpg \
--temp-dir /tmp
```

#### Decode dataset from tfrecords to images (debug)
```
docker run \
-v ${PWD}/dataset:/dataset \
-v ${PWD}/trained_models:/trained_models \
-v ${PWD}/deep_coffee:/src/deep_coffee \
-v ${PWD}/keras_pretrained_models:/root/.keras/models/ \
--rm --gpus all deep_coffee \
python -m deep_coffee.ml.decode_tfrecord_dataset \
--tfrecord_file "/dataset/tfrecords/train*" \
--output_dir /dataset/decoded_tfrecords \
--tft_artifacts_dir /dataset/tft_artifacts
```

#### Train network

**BEWARE** when training **ResNet** or **VGG** locally on your computer, you're likely to get OOM. Choose the batch size wisely.

``` 
export KERAS_HOME=/trained_models

docker run \
-v ${PWD}/dataset:/dataset \
-v ${PWD}/trained_models:/trained_models \
-v ${PWD}/deep_coffee:/src/deep_coffee \
-v ${PWD}/keras_pretrained_models:/root/.keras/models/ \
--rm --gpus all deep_coffee \
python -m deep_coffee.ml.train_and_evaluate \
--output_dir /trained_models \
--tft_artifacts_dir /dataset/tft_artifacts \
--input_dim 224 \
--trainset_len 1265 \
--evalset_len 264 \
--testset_len 278 \
--config_file /app/deep_coffee/ml/config/coffee_net_v1.yml \
--learning_rate 0.0001 \
--batch_size 8
```

#### Project Embeddings on TensorBoard

```
docker run \
-v ${PWD}/dataset:/dataset \
-v ${PWD}/trained_models:/trained_models \
-v ${PWD}/deep_coffee:/src/deep_coffee \
-v ${PWD}/keras_pretrained_models:/root/.keras/models/ \
--rm --gpus all deep_coffee \
python -m deep_coffee.ml.project_embeddings \
--tfrecord_path "/dataset/tfrecords/eval*" \
--output_dir /trained_models/coffee_net_v1/20200112-210616 \
--ckpt_path /trained_models/coffee_net_v1/20200112-210616/model.hdf5 \
--tft_artifacts_dir /dataset/tft_artifacts \
--layer_name head_dense_1 \
--dataset_len 264 \
--input_dim 224
```

#### Make prediction from a SavedModel

```
docker run \
-v ${PWD}/dataset:/dataset \
-v ${PWD}/trained_models:/trained_models \
-v ${PWD}/deep_coffee:/src/deep_coffee \
-v ${PWD}/keras_pretrained_models:/root/.keras/models/ \
--rm --gpus all deep_coffee \
python -m deep_coffee.ml.load_and_test_saved_model \
--model_path /trained_models/coffee_net_v1/20200112-230339/saved_model/ \
--sample_image_path /dataset/bad/*.jpg
```
